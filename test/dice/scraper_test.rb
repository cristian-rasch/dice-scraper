require File.expand_path("test/test_helper")
require File.expand_path("lib/dice/scraper")

module Dice
  class ScraperTest < Minitest::Test
    def setup
      @dice = Scraper.new
      @keywords = "web designer"
    end

    def test_getting_one_job
      jobs = @dice.job_search(@keywords, limit: 1)
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @dice.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_max_page_option
      Scraper.const_set("SEARCH_URL", Scraper::SEARCH_URL.sub("100", "10"))
      jobs = @dice.job_search(@keywords, max_page: 1, limit: 11)
      assert_operator jobs.size, :<, 20
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @dice.job_search(@keywords, date_range: 7, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
