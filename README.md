# Dice::Scraper

dice.com job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'dice-scraper', git: 'git@bitbucket.org:cristian-rasch/dice-scraper.git', require: 'dice/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "dice/scraper"
require "pp"

dice = Dice::Scraper.new
# Options include:
#   - date_rage - how far back to search for results (defaults to 15 days, valid values are 3, 7, 10, 15, 20, 25 and 30)
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
#   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/)
#               defaults to reading them from the DICE_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to
jobs = dice.job_search("web developer", limit: 3) # returns up to 3 jobs
pp jobs.first
# #<struct Dice::Scraper::Job
#  title="Web Developer",
#  url=
#   "https://www.dice.com/jobs/detail/Web-Developer-Innovar-Group-Greenwood-Village-CO-80111/10109766/SSCSWD",
#  employer="Innovar Group",
#  location="Greenwood Village, CO",
#  created_at=2015-02-13 23:04:03 UTC,
#  id="10109766",
#  salary="market",
#  skills="CSS3, SQL, J2EE, HTML5",
#  description_html=
#   "<p><strong>NEED TO BE LOCAL TO COLORADO</strong></p>\n<p>Innovargroup has an immediate opening for Web Developer – can work remotely or at the Greenwood Village office. This is a 1 year contract position.</p>\n<p> The ideal candidate is a self-organized software engineer with experience gathering requirements, designing, developing and deploying integrated software solutions. As a member of the Application Security team, you will be contributing to a team of highly motivated and some of the smartest minds in application security and be responsible for initiatives that help secure applications and data.  Qualifications </p>\n<p>• 5+ years’ experience in software development.  • Solid working knowledge and experience in HTML5, CSS3, SQL, J2EE &amp; web services.  • Some experience developing mobile applications (iOS and/or Android). </p>\n<p>Innovar Group is comprised of senior talent agents who deliver top recruitment services to clients throughout the United States. We bring a new era of recruiting to the industry by aligning state-of-the-art technology w/ outstanding talent.</p>">
# => #<struct Dice::Scraper::Job title="Web Developer", url="https://www.dice.com/jobs/detail/Web-Developer-Innovar-Group-Greenwood-Village-CO-80111/10109766/SSCSWD", employer="Innovar Group", location="Greenwood Village, CO", created_at=2015-02-13 23:04:03 UTC, id="10109766", salary="market", skills="CSS3, SQL, J2EE, HTML5", description_html="<p><strong>NEED TO BE LOCAL TO COLORADO</strong></p>\n<p>Innovargroup has an immediate opening for Web Developer – can work remotely or at the Greenwood Village office. This is a 1 year contract position.</p>\n<p> The ideal candidate is a self-organized software engineer with experience gathering requirements, designing, developing and deploying integrated software solutions. As a member of the Application Security team, you will be contributing to a team of highly motivated and some of the smartest minds in application security and be responsible for initiatives that help secure applications and data.  Qualifications </p>\n<p>• 5+ years’ experience in software development.  • Solid working knowledge and experience in HTML5, CSS3, SQL, J2EE &amp; web services.  • Some experience developing mobile applications (iOS and/or Android). </p>\n<p>Innovar Group is comprised of senior talent agents who deliver top recruitment services to clients throughout the United States. We bring a new era of recruiting to the industry by aligning state-of-the-art technology w/ outstanding talent.</p>">
```

