require_relative "scraper/version"
require "nokogiri"
require "open-uri"
require "dotenv"
Dotenv.load
require "English"
require "chronic"
require "logger"

module Dice
  class Scraper
    SEARCH_URL = "https://www.dice.com/jobs/q-%28:keywords%29-jtype-Contracts-sort-relevance-postedDate-:date_range-startPage-:page-limit-100-jobs.html".freeze
    DEFAULT_LIMIT = 100
    MAX_PAGE = 25
    DEFAULT_DATE_RANGE = 15
    VALID_DATE_RANGES = [3, 7, 10, 15, 20, 25, 30].freeze

    class Job < Struct.new(:title, :url, :employer, :location, :created_at, :id, :salary, :skills, :description_html); end

    # Options include:
    #   - date_rage - how far back to search for results (defaults to 15 days, valid values are 3, 7, 10, 15, 20, 25 and 30)
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the DICE_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - date_rage - how far back to search for results (defaults to 15 days, valid values are 3, 7, 10, 15, 20, 25 and 30)
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/)
    #               defaults to reading them from the DICE_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])
      configure_proxies(options[:proxies])

      limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
      limit = [limit, DEFAULT_LIMIT].min

      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      max_page = [max_page, MAX_PAGE].min

      kws = CGI.escape(Array(keywords).map(&:downcase).join(" OR "))
      search_url = SEARCH_URL.sub(":keywords", kws)

      date_range = if options.key?(:date_range)
                     options[:date_range]
                   elsif @options.key?(:date_range)
                     @options[:date_range]
                   end
      date_range = (VALID_DATE_RANGES & [date_range]).first || DEFAULT_DATE_RANGE
      search_url.sub!(":date_range", date_range.to_s)

      jobs = []
      page = 0

      begin
        page += 1
        jobs_url = search_url.sub(":page", page.to_s)
        io = open_through_proxy(jobs_url)
        next unless io

        search_results_page = Nokogiri::HTML(io)
        search_results_page.css(".serp-result-content").each do |search_result_node|
          title_link = search_result_node.at_css(".dice-btn-link")
          title = title_link.text
          job_url = title_link["href"]
          employer = search_result_node.at_css(".employer .dice-btn-link").text
          location = search_result_node.at_css(".location").text
          created_at = Chronic.parse(search_result_node.at_css(".posted").text).utc

          if io = open_through_proxy(URI.escape(job_url))
            search_result_page = Nokogiri::HTML(io)

            if company_info_node = search_result_page.at_css(".company-header-info")
              id = company_info_node.text[/Position Id : (\w+)\s/, 1]
              pay_rate = search_result_page.at_css(".mL20").text
              skills = search_result_page.css(".iconsiblings")[1].text
              description_html = search_result_page.at_css("#jobdescSec").inner_html.strip
            else
              job_overview_dts = search_result_page.css(".job_overview dt")
              id = find_data_in(job_overview_dts, /Position ID/)
              pay_rate = find_data_in(job_overview_dts, /Pay Rate/)
              skills = find_data_in(job_overview_dts, /Skills/)
              description_html = search_result_page.at_css(".dc_content").inner_html.strip
            end
          else
            id = pay_rate = skills = description_html = nil
          end

          job = Job.new(title, job_url, employer, location, created_at, id, pay_rate, skills, description_html)
          yield(job) if block_given?
          jobs << job

          break if jobs.size == limit
        end
      end while jobs.size < limit && page < max_page

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def find_data_in(dts, regexp)
      dts.find { |node| node.text =~ regexp }.next_element.text
    end

    def configure_proxies(proxies = nil)
      @proxies = (proxies || @options[:proxies] || ENV.fetch("DICE_PROXIES").split(",")).shuffle
    end

    def open_through_proxy(url)
      begin
        open(url, proxy: next_proxy)
      rescue => err
        @logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    def next_proxy
      @proxies.shift.tap { |proxy| @proxies << proxy }
    end
  end
end
